import org.testng.annotations.Test;
import pages.*;
import static org.testng.Assert.*;

public class CartTest {
    IndexPage indexPage = new IndexPage();
    CatalogPage catalogPage = new CatalogPage();
    ProductCardPage productCardPage = new ProductCardPage();
    CartPage cartPage = new CartPage();

    @Test
    public void testCart() throws InterruptedException {
        indexPage
                .openMainPage()
                .chooseMotherboardsCategory();
        catalogPage
                .setSocket1200Filter()
                .addItemToCart(5)
                .chooseCasesCategory()
                .setAeroCoolFilter()
                .addItemToCart(4)
                .openProductCard(10);
        productCardPage
                .addToCart()
                .goToCart();

        assertTrue(cartPage.isCartNotEmpty());
        assertEquals(CatalogPage.addedItemNames, cartPage.getItemList());
    }
}

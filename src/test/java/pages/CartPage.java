package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;

public class CartPage extends BasePage {
    static final String counterSpanXpath = "//span[contains(@class, 'PageTitle_count')]";
    static final String cartItemLinksXpath = "//div[@class='BasketItem_titleWrap__1d076']/a";

    public boolean isCartNotEmpty() {
        return driver.findElement(By.xpath(counterSpanXpath)).isDisplayed();
    }

    public List<String> getItemList() {
        List<String> itemList = new ArrayList<>();
        List<WebElement> cartItemLinks = driver.findElements(By.xpath(cartItemLinksXpath));

        for (WebElement cartItemLink : cartItemLinks) {
            itemList.add(cartItemLink.getText());
        }

        return itemList;
    }
}

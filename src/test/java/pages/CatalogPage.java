package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import java.util.ArrayList;
import java.util.List;

public class CatalogPage extends BasePage {
    static final String socket1200LabelXpath = "//label[@for='id-LGA-1200-23313'][text()='LGA 1200']";
    static final String aeroCoolLabelXpath = "//label[@for='id-AeroCool-AeroCool'][text()='AeroCool']";
    static final String addItemToCartButtonsXpath = "//button[contains(@class, 'Button_inGridCard__2P3o8')]";
    static final String casesLinkXpath = "//a[text()='Корпуса']";
    static final String productCardLinksXpath = "//div[contains(@class, 'CardText_listing')]/a[contains(@class, 'CardText_link__2H3AZ')]";
    public static List<String> addedItemNames = new ArrayList<>();

    public CatalogPage setSocket1200Filter() {
        WebElement socket1200Label = driver.findElement(By.xpath(socket1200LabelXpath));
        socket1200Label.click();

        return this;
    }

    public CatalogPage addItemToCart(int number) throws InterruptedException {
        List<WebElement> addItemToCartButtons = driver.findElements(By.xpath(addItemToCartButtonsXpath));
        int i = number - 1;
        Actions actions = new Actions(driver);
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;

        jsExecutor.executeScript("arguments[0].scrollIntoView();", addItemToCartButtons.get(i));
        // DOM после скролла похоже перестраивается, поэтому:
        addItemToCartButtons = driver.findElements(By.xpath(addItemToCartButtonsXpath));
        actions.moveToElement(addItemToCartButtons.get(i)).perform();
        Thread.sleep(500); // без этого Selenium упорно считает, что кнопка перекрывается элементом который вообще справа и не может быть над ней. и не кликабельна
        // и после hover'а тоже все протухает
        addItemToCartButtons = driver.findElements(By.xpath(addItemToCartButtonsXpath));
        List<WebElement> productCardLinks = driver.findElements(By.xpath(productCardLinksXpath)); // это чтобы запомнить наименования товаров
        addedItemNames.add(productCardLinks.get(i).getText());
        addItemToCartButtons.get(i).click();

        return this;
    }

    public CatalogPage chooseCasesCategory() {
        WebElement catalogButton = driver.findElement(By.xpath(catalogButtonXpath));
        catalogButton.click();
        WebElement casesLink = driver.findElement(By.xpath(casesLinkXpath));
        casesLink.click();

        return this;
    }

    public CatalogPage setAeroCoolFilter() {
        WebElement aeroCoolLabel = driver.findElement(By.xpath(aeroCoolLabelXpath));
        aeroCoolLabel.click();

        return this;
    }

    public void openProductCard(int number) {
        List<WebElement> productCardLinks = driver.findElements(By.xpath(productCardLinksXpath));
        int i = number - 1;
        addedItemNames.add(productCardLinks.get(i).getText());
        productCardLinks.get(i).click();
    }
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class IndexPage extends BasePage {
    static final String motherboardsLinkXpath = "//a[text()='Материнские платы']";

    public IndexPage openMainPage() {
        super.open();

        return this;
    }

    public void chooseMotherboardsCategory() {
        WebElement catalogButton = driver.findElement(By.xpath(catalogButtonXpath));
        catalogButton.click();
        WebElement motherboardsLink = driver.findElement(By.xpath(motherboardsLinkXpath));
        motherboardsLink.click();
    }
}

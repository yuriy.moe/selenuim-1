package pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import java.time.Duration;

public class BasePage {
    static WebDriver driver;
    static final String url = "https://www.regard.ru/";
    static final String catalogButtonXpath = "//button[contains(@class, 'Button_primaryRed__3-T9I')]";

    public BasePage() {
        driver = WebDriverManager.firefoxdriver().create();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.manage().window().maximize();
    }

    public void open() {
        driver.get(url);
    }
}

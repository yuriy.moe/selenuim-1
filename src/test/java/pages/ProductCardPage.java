package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ProductCardPage extends BasePage {
    static final String addToCartButtonXpath = "//button[contains(@class, 'ProductActionBlock_cartButton')]";
    static final String cartButtonXpath = "//button[contains(@class, 'ProductActionBlock_cartButton_inCart')]";

    public ProductCardPage addToCart() {
        WebElement addToCartButton = driver.findElement(By.xpath(addToCartButtonXpath));
        new Actions(driver).moveToElement(addToCartButton).perform(); // без этого Selenium иногда считает, что кнопка чем-то перекрывается
        addToCartButton.click();

        return this;
    }

    public void goToCart() {
        WebElement cartButton = driver.findElement(By.xpath(cartButtonXpath));
        cartButton.click();
    }
}
